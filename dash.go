// Command dashboard implements basic server to display feed of events received
// from remote servers
package main

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/gob"
	"encoding/hex"
	"flag"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	mrand "math/rand"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
	"unicode/utf8"

	"golang.org/x/crypto/bcrypt"

	"github.com/artyom/autoflags"
	"github.com/artyom/basicauth"
	"github.com/artyom/httpflags"
	"github.com/boltdb/bolt"
	"github.com/golang/snappy"
)

func main() {
	args := struct {
		Addr string `flag:"addr,address to listen"`
		File string `flag:"db,path to database file"`
		Days int    `flag:"days,max duration to keep events"`
		Dev  bool   `flag:"insecure,don't mark cookies with secure flag"`
	}{
		Addr: "localhost:8080",
		File: "/tmp/dashboard.db",
		Days: 1,
	}
	autoflags.Define(&args)
	flag.Parse()

	if err := serve(args.Addr, args.File, args.Days, args.Dev); err != nil {
		log.Fatal(err)
	}
}

func serve(addr, file string, retention int, insecure bool) error {
	dash, err := openDashboard(file, retention, log.New(os.Stderr, "", log.LstdFlags))
	if err != nil {
		return err
	}
	defer dash.Close()
	dash.insecure = insecure
	srv := http.Server{
		Addr:              addr,
		Handler:           dash,
		ReadHeaderTimeout: time.Second,
		IdleTimeout:       time.Minute,
	}
	go func() {
		sigCh := make(chan os.Signal, 1)
		signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)
		<-sigCh
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		srv.Shutdown(ctx)
		cancel()
	}()
	switch err := srv.ListenAndServe(); err {
	case http.ErrServerClosed:
		return nil
	default:
		return err
	}
}

func openDashboard(file string, retentionDays int, logger *log.Logger) (*dash, error) {
	db, err := bolt.Open(file, 0644, &bolt.Options{Timeout: time.Second})
	if err != nil {
		return nil, err
	}
	d := &dash{
		db:          db,
		logger:      logger,
		submitRealm: basicauth.NewRealm("restricted"),
		admins:      make(map[string][]byte),
		macKey:      make([]byte, 32),
		cancel:      func() {},
	}
	rand.Read(d.macKey)
	if err := d.populateRealms(); err != nil {
		d.Close()
		return nil, err
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/", d.requireAdmin(d.handleEventsView))
	mux.HandleFunc("/add", d.submitRealm.WrapHandlerFunc(d.handleEventAdd))
	d.handler = withSecureHeaders(mux)
	if retentionDays > 0 {
		ctx, cancel := context.WithCancel(context.Background())
		d.cancel = cancel
		go d.purgeOld(ctx, retentionDays)
	}
	return d, nil
}

func (d *dash) requireAdmin(f http.HandlerFunc) http.HandlerFunc {
	const cookieName = "du"
	return func(w http.ResponseWriter, r *http.Request) {
		if ck, err := r.Cookie(cookieName); err == nil && d.validToken(ck.Value) {
			f(w, r)
			return
		}
		if r.Method == http.MethodPost {
			if err := r.ParseForm(); err != nil {
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}
			name := r.PostForm.Get("name")
			if d.validCredentials(name, r.PostForm.Get("pass")) {
				http.SetCookie(w, d.newCookie(cookieName, name))
				http.Redirect(w, r, r.URL.Path, http.StatusFound)
				return
			}
		}
		http.SetCookie(w, &http.Cookie{Name: cookieName, MaxAge: -1, HttpOnly: true})
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(loginPageBody))
	}
}

func (d *dash) validCredentials(name, pass string) bool {
	if name == "" || pass == "" {
		return false
	}
	var wrong []byte
	d.mu.Lock()
	b := d.admins[name]
	for _, wrong = range d.admins { // grab any non-empty value
		break
	}
	d.mu.Unlock()
	if b == nil {
		if wrong != nil {
			// check against any stored hash to avoid timing attacks
			// guessing login
			_ = bcrypt.CompareHashAndPassword(wrong, []byte(pass))
		}
		return false
	}
	return bcrypt.CompareHashAndPassword(b, []byte(pass)) == nil
}

func (d *dash) newCookie(name, msg string) *http.Cookie {
	vals := make(url.Values)
	vals.Set("u", msg)
	mac := hmac.New(sha256.New, d.macKey)
	mac.Write([]byte(msg))
	vals.Set("t", base64.RawStdEncoding.EncodeToString(mac.Sum(nil)))
	return &http.Cookie{
		Name:     name,
		Value:    vals.Encode(),
		Expires:  time.Now().AddDate(0, 6, 0),
		HttpOnly: true,
		Secure:   !d.insecure,
	}
}

func (d *dash) validToken(s string) bool {
	if s == "" {
		return false
	}
	vals, err := url.ParseQuery(s)
	if err != nil {
		return false
	}
	u, t := vals.Get("u"), vals.Get("t")
	if u == "" || t == "" {
		return false
	}
	d.mu.Lock()
	_, ok := d.admins[u]
	d.mu.Unlock()
	messageMAC, err := base64.RawStdEncoding.DecodeString(t)
	if err != nil {
		return false
	}
	mac := hmac.New(sha256.New, d.macKey)
	mac.Write([]byte(u))
	expectedMAC := mac.Sum(nil)
	return hmac.Equal(messageMAC, expectedMAC) && ok
}

func (d *dash) ServeHTTP(w http.ResponseWriter, r *http.Request) { d.handler.ServeHTTP(w, r) }

func (d *dash) populateRealms() error {
	return d.db.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists([]byte(viewersBucket))
		if err != nil {
			return err
		}
		cnt := 0
		fn := func(k, v []byte) error {
			cnt++
			vc := make([]byte, len(v))
			copy(vc, v)
			d.admins[string(k)] = vc
			return nil
		}
		if err := bkt.ForEach(fn); err != nil {
			return err
		}
		if cnt == 0 {
			user, pass := "admin", randomString(20)
			passb, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
			if err != nil {
				return err
			}
			d.admins[user] = passb
			if err := bkt.Put([]byte(user), passb); err != nil {
				return err
			}
			d.logf("new viewer user created: %q, %q", user, pass)
		}
		bkt, err = tx.CreateBucketIfNotExists([]byte(submittersBucket))
		if err != nil {
			return err
		}
		cnt = 0
		fn = func(k, v []byte) error {
			cnt++
			vc := make([]byte, len(v))
			copy(vc, v)
			return d.submitRealm.AddUserHashed(string(k), vc)
		}
		if err := bkt.ForEach(fn); err != nil {
			return err
		}
		if cnt == 0 {
			user, pass := "service", randomString(20)
			passb, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
			if err != nil {
				return err
			}
			d.submitRealm.AddUserHashed(user, passb)
			if err := bkt.Put([]byte(user), passb); err != nil {
				return err
			}
			d.logf("new submitter user created: %q, %q", user, pass)
		}
		return nil
	})
}

func randomString(n int) string {
	const symbols = `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-_=#$%^&*@!()`
	b := make([]byte, n)
	for i := range b {
		b[i] = symbols[mrand.Intn(len(symbols))]
	}
	return string(b)
}

func init() { mrand.Seed(time.Now().UnixNano() + int64(os.Getpid())) }

type dash struct {
	db      *bolt.DB
	logger  *log.Logger
	handler http.Handler

	submitRealm *basicauth.Realm
	mu          sync.Mutex
	admins      map[string][]byte
	macKey      []byte
	insecure    bool // if true, don't mark cookies as secure

	cancel context.CancelFunc
}

func (d *dash) Close() error { d.cancel(); return d.db.Close() }

func (d *dash) purgeOld(ctx context.Context, retentionDays int) {
	if retentionDays < 1 {
		return
	}
	ticker := time.NewTicker(5 * time.Minute)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case t := <-ticker.C:
			max := t.UTC().AddDate(0, 0, -retentionDays).AppendFormat(nil, time.RFC3339)
			cnt := 0
			err := d.db.Update(func(tx *bolt.Tx) error {
				bkt := tx.Bucket([]byte(eventsBucket))
				if bkt == nil {
					return nil
				}
				c := bkt.Cursor()
				for k, _ := c.First(); k != nil && bytes.Compare(k, max) <= 0; k, _ = c.Next() {
					if err := c.Delete(); err != nil {
						return err
					}
					cnt++
				}
				return nil
			})
			switch {
			case err == nil && cnt > 0:
				d.logf("removed %d old events", cnt)
			case err != nil:
				d.logf("error removing old events: %v", err)
			}
		}
	}
}

func (d *dash) handleEventsView(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.Header().Set("Allow", "GET")
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	args := struct {
		Last  time.Duration `flag:"last"`
		Start fTime         `flag:"start"`
		End   fTime         `flag:"end"`
	}{Last: 24 * time.Hour}
	if err := httpflags.Parse(&args, r); err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	var start, end time.Time
	var useLast bool
	now := time.Now().UTC()
	if time.Time(args.Start).IsZero() && time.Time(args.End).IsZero() {
		if args.Last <= 0 {
			args.Last = 24 * time.Hour
		}
		useLast = true
		start, end = now.Add(-args.Last), now
	} else {
		start, end = time.Time(args.Start), time.Time(args.End)
	}
	events, err := d.getEvents(start, end)
	if err != nil {
		d.log(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if useLast {
		w.Header().Set("Refresh", "1800")
	}
	if len(events) == 0 {
		w.WriteHeader(http.StatusNotFound)
	}
	viewEventsTemplate.Execute(w, struct {
		Title      string
		Events     []*Event
		UseLast    bool
		Last       time.Duration
		Start, End time.Time
	}{
		Title:   "Events",
		Events:  events,
		Last:    args.Last,
		UseLast: useLast,
		Start:   start,
		End:     end,
	})
}

func (d *dash) handleEventAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.Header().Set("Allow", "POST")
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	buf := new(bytes.Buffer)
	n, err := io.Copy(buf, io.LimitReader(r.Body, 64<<10+50))
	if err != nil {
		d.log(err)
	}
	if err != nil || n == 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	io.Copy(ioutil.Discard, r.Body)
	evt := &Event{
		Time: time.Now().UTC(),
		Host: r.Header.Get("X-Hostname"),
	}
	switch {
	case utf8.Valid(buf.Bytes()):
		evt.Text = buf.String()
	default:
		evt.Text = hex.Dump(buf.Bytes())
		evt.Binary = true
	}
	if err := d.addEvent(buf, evt); err != nil {
		d.log(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (d *dash) addEvent(buf *bytes.Buffer, evt *Event) error {
	data, err := encodeEvent(buf, evt)
	if err != nil {
		return err
	}
	key := newKey(evt.Time)
	return d.db.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists([]byte(eventsBucket))
		if err != nil {
			return err
		}
		return bkt.Put(key, data)
	})
}

func (d *dash) getEvents(start, end time.Time) ([]*Event, error) {
	if end.Before(start) {
		start, end = end, start
	}
	min := start.UTC().AppendFormat(nil, time.RFC3339)
	max := end.UTC().AppendFormat(nil, time.RFC3339)
	var events []*Event
	err := d.db.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(eventsBucket))
		if bkt == nil {
			return nil
		}
		c := bkt.Cursor()
		k, v := c.Seek(max)
		if k == nil { // max is out of range, need to take last element then
			k, v = c.Last()
		}
		for ; k != nil && bytes.Compare(k, min) > 0 && bytes.Compare(k, max) <= 0; k, v = c.Prev() {
			evt, err := decodeEvent(v)
			if err != nil {
				return err
			}
			events = append(events, evt)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return events, nil
}

const (
	eventsBucket     = "events"
	viewersBucket    = "viewers"
	submittersBucket = "submitters"
)

func newKey(t time.Time) []byte {
	b := make([]byte, len(time.RFC3339)+1+8)
	i := len(t.UTC().AppendFormat(b[:0], time.RFC3339))
	b[i] = '#'
	b2 := make([]byte, 4)
	rand.Read(b2)
	hex.Encode(b[i+1:], b2)
	return b[:i+1+8]
}

func decodeEvent(b []byte) (*Event, error) {
	dec := gob.NewDecoder(snappy.NewReader(bytes.NewReader(b)))
	evt := new(Event)
	if err := dec.Decode(evt); err != nil {
		return nil, err
	}
	return evt, nil
}

func encodeEvent(buf *bytes.Buffer, evt *Event) ([]byte, error) {
	if buf == nil {
		buf = new(bytes.Buffer)
	}
	buf.Reset()
	sw := snappy.NewBufferedWriter(buf)
	if err := gob.NewEncoder(sw).Encode(evt); err != nil {
		return nil, err
	}
	if err := sw.Close(); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (d *dash) log(v ...interface{}) {
	if d.logger != nil {
		d.logger.Println(v...)
	}
}

func (d *dash) logf(format string, v ...interface{}) {
	if d.logger != nil {
		d.logger.Printf(format, v...)
	}
}

type Event struct {
	Time time.Time
	Host string
	Text string

	Binary bool // whether text is converted from binary
}

// fTime implements flag.Value so it can be used with httpflags
type fTime time.Time

func (t *fTime) String() string { return time.Time(*t).String() }
func (t *fTime) Set(value string) error {
	if value == "now" {
		*t = fTime(time.Now().UTC())
		return nil
	}
	formats := []string{
		"2006-01-02T15:04:05",
		"2006-01-02",
	}
	var t2 time.Time
	var err error
	for _, f := range formats {
		if t2, err = time.Parse(f, value); err == nil {
			*t = fTime(t2)
			return nil
		}
	}
	return err
}

func withSecureHeaders(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Robots-Tag", "noindex, nofollow")
		w.Header().Set("X-Frame-Options", "DENY")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("X-XSS-Protection", "1; mode=block")
		w.Header().Set("Content-Security-Policy", "default-src 'self'; style-src 'unsafe-inline'")
		h.ServeHTTP(w, r)
	})
}

var viewEventsTemplate = template.Must(template.New("viewEvents").Parse(viewEventsTemplateText))

const viewEventsTemplateText = `<!doctype html>
<head><title>{{.Title}}</title><meta charset=utf-8>
<meta name=viewport content="width=device-width, initial-scale=1.0">
<style>
body {
	font-family: "Go", sans-serif;
	font-size: 100%;
	line-height: 170%;
	max-width: 80em;
	margin: auto;
	padding-right: 1em;
	padding-left: 1em;
}
samp, pre {
	font-family: "Go Mono", monospace;
}
pre {
	line-height: 140%;
	background-color:#eee;
	padding: 0.5em;
	overflow: auto;
}
</style>
</head>
<h1>{{if .Events}}E{{else}}No e{{end}}vents {{if .UseLast}} for the last {{.Last}}{{else}} from {{.Start.Format "2006-01-02 15:04:05"}} to {{.End.Format "2006-01-02 15:04:05"}}{{end}}</h1>
{{range .Events}}
<article><p><time>{{.Time.Format "2006-01-02 15:04:05"}}</time>{{if .Host}} from <strong>{{.Host}}</strong>{{end}}:</p>
{{if .Binary}}<details><summary><strong>Binary data</strong></summary>
<pre><samp>{{.Text}}</samp></pre>
</details>
{{else}}
<pre><samp>{{.Text}}</samp></pre>
{{end}}
</article>
{{end}}
`

const loginPageBody = `<!doctype html>
<head><title>Login</title><meta charset=utf-8>
<form method=post autocomplete=on><input type=text name=name autofocus required>
<input type=password name=pass required>
<input type=submit></form>
`
